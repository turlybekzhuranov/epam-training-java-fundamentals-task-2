import java.util.SplittableRandom;

public class Main {
    public static void main(String[] args){
        int[] numbers = getNumbers();
        printShortestAndLongestNumber(numbers);
        printFirstNumberWithMinimalDifferentDigitsCount(numbers);
        printStrictAscendingOrderNumber(numbers);
        printFirstDigitsDifferentNumber(numbers);
    }

    static int[] getNumbers(){
        Requester requester = new Requester();
        String[] numbers = requester.requestLine("Enter numbers (separated by space):").trim().split("\\s+");
        int[] validNumbers = new int[numbers.length];
        for (int i=0; i<numbers.length; i++){
            if (isNumeric(numbers[i])){
                validNumbers[i] = Integer.parseInt(numbers[i]);
            } else {
                return getNumbers();
            }
        }

        requester.scanner.close();
        return validNumbers;
    }

    static boolean isNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }

    static void printShortestAndLongestNumber(int[] numbers){
        int minLength, maxLength, shortestNumber, longestNumber;
        minLength = maxLength = (numbers[0] == 0) ? 1 : (int)(Math.log10(Math.abs(numbers[0])) + 1);
        shortestNumber = longestNumber = numbers[0];
        for (int i=1; i<numbers.length; i++){
            int length = (numbers[i] == 0) ? 1 : (int)(Math.log10(Math.abs(numbers[i])) + 1);
            if (minLength > length){
                minLength = length;
                shortestNumber = numbers[i];
            }
            if (maxLength < length){
                maxLength = length;
                longestNumber = numbers[i];
            }
        }
        System.out.println("The shortest number is " + shortestNumber + " with length " + minLength);
        System.out.println("The longest number is " + longestNumber + " with length " + maxLength);
    }

    static int getDifferentDigitsCount(int number){
        int differentDigitsCount = 0;
        int[] digitsCount = new int[10];
        while (number != 0){
            digitsCount[number%10]++;
            number /= 10;
        }

        for (int i=0; i<10; i++){
            if (digitsCount[i] > 0) {
                differentDigitsCount++;
            }
        }

        return differentDigitsCount;
    }

    static void printFirstNumberWithMinimalDifferentDigitsCount(int[] numbers){
        int minDifferentDigitsCount = String.valueOf(Math.abs(numbers[0])).length(), minDifferentDigitsNumber = numbers[0];

        for (int i=1; i<numbers.length; i++){
            int differentDigitsCount = getDifferentDigitsCount(Math.abs(numbers[i]));
            if (minDifferentDigitsCount > differentDigitsCount){
                minDifferentDigitsCount = differentDigitsCount;
                minDifferentDigitsNumber = numbers[i];
            }
        }
        System.out.println("First number with minimal different digits is " + minDifferentDigitsNumber);
    }

    static boolean isStrictAscendingOrder(int number){
        if (number < 10){
            return true;
        }
        int lastDigit = number%10;
        number /= 10;
        while (number != 0){
            if (lastDigit <= number%10){
                return false;
            }
            lastDigit = number%10;
            number /= 10;
        }
        return true;
    }
    static void printStrictAscendingOrderNumber(int[] numbers){
        String output = "There is no strict ascending order number";
        for (int number: numbers){
            if (isStrictAscendingOrder(Math.abs(number))){
                output = "First strict ascending order number is " + number;
                break;
            }
        }
        System.out.println(output);
    }

    static boolean isDigitsDifferent(String number){
        if (number.length() < 2){
            return true;
        }

        for (int i=0; i<number.length()-1; i++){
            for (int j=1; j<number.length(); j++){
                if (number.charAt(i) == number.charAt(j)){
                    return false;
                }
            }
        }
        return true;
    }

    static void printFirstDigitsDifferentNumber(int[] numbers){
        String output = "There is no digits different number";
        for (int number: numbers){
            if (isDigitsDifferent(String.valueOf(Math.abs(number)))){
                output = "First digits different number is " + number;
                break;
            }
        }
        System.out.println(output);
    }
}
